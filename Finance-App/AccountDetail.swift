//
//  AccountDetail.swift
//  Finance-App
//
//  Created by David Bielenberg on 12.01.20.
//  Copyright © 2020 David Bielenberg. All rights reserved.
//

import SwiftUI

struct AccountDetail: View {
    
    @State var progressTransactions: Float = 0
    @State var progressDisposals: Float = 0
    
    var body: some View {
        VStack(alignment: .leading) {
            Text("Your Mastercard, David")
                .font(.title)
                .fontWeight(.heavy)
                .multilineTextAlignment(.leading)
                //.padding(.leading, 8)
            
            //            PaymentCardView(account: accounts[0])
            //                .rotationEffect(.degrees(-90.0))
            
            Text("Card Settings")
                .font(.headline)
                .fontWeight(.bold)
            
            CardOptionForm(label: "Visible")
            CardOptionForm(label: "Card is active")
            CardOptionForm(label: "Contactless payments")
            
            Text("Daily Transaction limit")
            Slider(value: $progressTransactions)
            Text("Daily Disposal limit")
            Slider(value: $progressDisposals)
            
        }.padding(.horizontal, 8)
        
    }
}

struct AccountDetail_Previews: PreviewProvider {
    static var previews: some View {
        AccountDetail()
    }
}

struct CardOptionForm: View {
    
    @State var isActive = true
    
    let label: String
    
    var body: some View {
        VStack {
            Toggle(isOn: $isActive) {
                HStack {
                    Image(systemName: "star.fill")
                        .font(.headline)
                    //.frame(width: 40, height: 40)
                    Text(label)
                }
            }
            .padding(.horizontal, 16)
            .frame(height: 80)
            .background(Color.gray)
            .cornerRadius(16)
        }
    }
}
