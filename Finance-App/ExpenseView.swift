//
//  ExpenseView.swift
//  Finance-App
//
//  Created by David Bielenberg on 12.01.20.
//  Copyright © 2020 David Bielenberg. All rights reserved.
//

import SwiftUI

struct ExpenseView: View {
    
    let expense: Expense
    
    func formatDate() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        
        return dateFormatter.string(from: expense.date)
    }
    
    
    var body: some View {
        HStack {
            Image(systemName: "star.fill")
                .frame(width: 40, height: 40)
                .foregroundColor(expense.positive ? Color("blue") : Color("red"))
                .background(expense.positive ? Color("blue").opacity(0.2) : Color("red").opacity(0.2))
                .cornerRadius(20)
            VStack(alignment: .leading) {
                Text(expense.title)
                    .font(.body)
                    .fontWeight(.bold)
                Text(self.formatDate() )
                    .font(.caption)
            }
            Spacer()
            Text("\(expense.positive ? "+" : "-") \(expense.amount, specifier: "%.2f")")
                .padding(8)
                .frame(width: 70, height: 40)
                .foregroundColor(expense.positive ? Color("blue") : Color("red"))
                .background(expense.positive ? Color("blue").opacity(0.2) : Color("red").opacity(0.2))
                .cornerRadius(4)
        }.padding(.vertical, 8)
    }
}
