//
//  Profile.swift
//  Finance-App
//
//  Created by David Bielenberg on 11.01.20.
//  Copyright © 2020 David Bielenberg. All rights reserved.
//

import SwiftUI

struct Profile: View {
    
    @State var option = true
    @State var darkmode = false
    
    var body: some View {
        VStack {
            Image(systemName: "person.fill")
            Text("Your Account")
            Spacer()
            Form {
                Toggle(isOn: $darkmode) {
                    Text("Dark Mode")
                }
                Toggle(isOn: $option) {
                    Text("Option 1")
                }
                Picker(selection: /*@START_MENU_TOKEN@*/.constant(1)/*@END_MENU_TOKEN@*/, label: Text("Currency")) {
                    Text("EUR").tag(1)
                    Text("USD").tag(2)
                    Text("BTC").tag(2)
                }
                Picker(selection: /*@START_MENU_TOKEN@*/.constant(1)/*@END_MENU_TOKEN@*/, label: Text("Language")) {
                    Text("German").tag(1)
                    Text("English").tag(2)
                    Text("Italian").tag(2)
                    Text("Spanish").tag(2)
                }
            }.frame(height: 300)
            
            Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/) {
                Spacer()
                Text("Sign out")
                Spacer()
            }
            .frame(height: 80)
            .background(Color.gray)
            .foregroundColor(Color.primary)
            .cornerRadius(16)
            .padding(16)
        }
    }
}

struct Profile_Previews: PreviewProvider {
    static var previews: some View {
        Profile()
    }
}
