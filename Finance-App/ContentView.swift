//
//  ContentView.swift
//  Finance-App
//
//  Created by David Bielenberg on 11.01.20.
//  Copyright © 2020 David Bielenberg. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        TabView() {
        Transactions().tabItem {
            Image(systemName: "star.fill")
            Text("Transactions") }.tag(1)
        Profile().tabItem {
            Image(systemName: "star.fill")
            Text("Profile") }.tag(2)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
