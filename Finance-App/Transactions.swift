import SwiftUI
import Combine

struct Transactions: View {
    
    @ObservedObject var settings = appSettings()
    
    @State var filter = false
    @State var selectedFilter = 1
    @State var selectedAccount = 1
    @State var filteredExpenses = expenses
    
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text("Your Accounts")
                    .font(.title)
                    .fontWeight(.bold)
                Spacer()
                Image("Avatar")
                    .resizable()
                    .frame(width: 48, height: 48)
                    .background(Color.gray)
                    .cornerRadius(48/2)
            }.padding(.horizontal, 16)
            Button(action: {
                //
            }) {
                Text("Switch Account")
            }
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 16) {
                    ForEach(settings.accounts) { item in
                        PaymentCardView(account: item)
                    }
                }
                .padding(.leading, 16)
            }
            HStack {
                Text("Transactions")
                    .font(.title)
                    .fontWeight(.bold)
                    .padding(.leading, 16)
                    .padding(.trailing, 16)
                Spacer()
                Button(action: {
                    if self.selectedFilter < 3 {
                        self.selectedFilter += 1
                    } else {
                        self.selectedFilter = 1
                    }
                    self.filteredExpenses = self.settings.getFilteredExpenses(filter: self.selectedFilter)
                }) {
                    Text("  \(self.selectedFilter == 1 ? "All" : "\(self.selectedFilter == 2 ? "Positive" : "Negative"  )")  ")
                        .padding(8)
                        .background(Color.gray).opacity(0.2)
                        .cornerRadius(8)
                }
                .padding(.horizontal, 16)
            }
            
            List(self.filteredExpenses) { item in
                ExpenseView(expense: item)
            }
            
        }
        .padding(.top, 16)
    }
    
    // MARK: FUNCTIONS OF TRANSACTIONS VIEW
    
    
}

struct Transactions_Previews: PreviewProvider {
    static var previews: some View {
        Transactions()
    }
}

struct PaymentCardView: View {
    
    let account: Account
    
    let sizeX = 200 as CGFloat
    let sizeY = 250 as CGFloat
    
    @State var showCopied: Bool = false
    @State var balanceText = "Current Balance"
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Text(balanceText)
                    .opacity(0.5)
                    .multilineTextAlignment(.leading)
                Text("\(account.balance, specifier: "%.2f")")
                    .font(.title)
                    .fontWeight(.medium)
                    .multilineTextAlignment(.leading)
                Spacer()
                Text(account.type)
            }
            .frame(width: sizeX, height: sizeY)
            .padding(.vertical, 16)
            .background(account.color)
            .cornerRadius(16)
            .shadow(color: account.color.opacity(0.2), radius: 10, x: 0.0, y: 10.0)
            .padding(.vertical, 32)
            .onTapGesture {
                self.showCopied.toggle()
            }
            .alert(isPresented: $showCopied, content: {
                Alert(title: Text("Information copied"), message: Text("Account infos are now in your clipboard."), dismissButton: .default(Text("OK")))
            })
        }
    }
}


