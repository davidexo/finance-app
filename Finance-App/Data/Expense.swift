
import Foundation
import SwiftUI

struct Expense: Identifiable {
    let id = UUID()
    let title: String
    let amount: Double
    let date = Date()
    let positive: Bool
}

var expenses = [
    Expense(title: "Drugs", amount: 10.30, positive: false),
    Expense(title: "Food", amount: 2.00, positive: true),
    Expense(title: "Stuff", amount: 15.14, positive: false),
    Expense(title: "Drugs", amount: 10.00, positive: false),
    Expense(title: "Food", amount: 5.50, positive: true),
    Expense(title: "Drugs", amount: 1.80, positive: false)
]

var expenses2 = [
    Expense(title: "Phone", amount: 10.30, positive: false),
    Expense(title: "Phone", amount: 2.00, positive: true),
    Expense(title: "Phone", amount: 15.14, positive: false)
]
