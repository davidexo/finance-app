
import Foundation
import SwiftUI
import Combine

struct Account: Identifiable {
    let id = UUID()
    let type: String
    let balance: Double
    let due = Date()
    let color: Color
    
    var items: [Expense]
    
    func returnNegative() -> [Expense] {
        var copy = self.items
        copy = items.filter { $0.positive == false }
        return copy
    }
    
    func returnPositive() -> [Expense] {
        var copy = self.items
        copy = items.filter { $0.positive }
        return copy
    }
    
    func returnAll() -> [Expense]{
        return items
    }
}


class appSettings: ObservableObject {
    
    var selectedAccount = 0
    
    // 0 = All
    // 1 = Positive Expenses
    // 2 = Negative Expenses
    @State var filteredExpenses = 1
    
    var accounts = [
        Account(type: "Visa", balance: 2023.02, color: Color.yellow, items: expenses),
        Account(type: "Mastercard", balance: 2023.02, color: Color.red, items: expenses2)
    ]
    
    func getActiveAccount() -> Account {
        return self.accounts[selectedAccount]
    }
    
    func switchAccount() {
        selectedAccount == 0 ? (selectedAccount = 1) : (selectedAccount = 0)
    }
    
    func getFilteredExpenses(filter: Int) -> [Expense] {
        if filter == 1 {
            return getActiveAccount().returnAll()
        } else if filter == 2 {
            return getActiveAccount().returnPositive()
        } else {
            return getActiveAccount().returnNegative()
        }
    }
    
    
    
    
}

